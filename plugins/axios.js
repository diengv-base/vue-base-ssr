export default function ({$axios, redirect}) {
  $axios.onRequest(config => {
    console.log('Making request to ' + config.url)
  })

  $axios.onRequestError(err => {
    console.warn('Request error: ', err)
  })

  $axios.onResponse(response => {

  })

  $axios.onResponseError(err => {
    console.warn('Response error: ', err)
  })

  $axios.onError(error => {
    if(error.response.status === 500) {
      redirect('/sorry')
    }
  })
}
