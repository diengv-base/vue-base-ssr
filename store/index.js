export const state = () => ({
  isOnline: true
})

export const mutations = {
  UPDATE_ONLINE_FLAG(state, flag) {
    state.isOnline = flag
  }
}


export const actions = {
  updateOnlineFlag({commit}, flag) {
    commit('UPDATE_ONLINE_FLAG', flag)
  }
}

export const getters = {}
