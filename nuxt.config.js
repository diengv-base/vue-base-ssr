import pkg from './package'
import webpack from 'webpack'

require('dotenv').config()

export default {
  mode: 'universal',
  buildDir: 'build',
  // globalName: 'omi',
  dev: (process.env.NODE_ENV !== 'production'),
  server: {
    port: 8000, // default: 3000
    host: 'localhost', // default: localhost,
    // timing: false,
    timing: {
      total: true
    },
    /*https: {
      key: fs.readFileSync(path.resolve(__dirname, 'server.key')),
      cert: fs.readFileSync(path.resolve(__dirname, 'server.crt'))
    }*/
    // socket: '/tmp/nuxt.socket'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: pkg.description}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },
  middleware: 'auth',
  router: {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'exact-is-active',
    linkPrefetchedClass: 'prefetched-is-active',
    middleware: 'user-agent'
  },
  transition: {
    name: 'page',
    mode: 'out-in',
    beforeEnter(el) {
      // console.log('Before enter...');
    }
  },
  layoutTransition: {
    name: 'layout',
    mode: 'out-in'
  },
  /*  vue: {
      config: {
        productionTip: false,
        devtools: true
      }
    },*/
  /*
  ** Customize the progress-bar color
  */
  // loading: false, disable
  loading: {
    color: 'blue',
    height: '5px'
  },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/main.css',
    '@/assets/scss/main.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/axios'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    ['@nuxtjs/dotenv', {
      systemvars: true,
      path: '/env/',
      filename: '.env.' + process.env.NODE_ENV
    }],
    '@nuxtjs/axios',
    // '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    '@nuxtjs/toast',
    ['nuxt-i18n', {
      vueI18nLoader: true,
      locales: [
        {code: 'en', iso: 'en-US', file: 'en-US.js'},
        {code: 'vi', iso: 'vi-VN', file: 'vi-VN.js'},
        {code: 'ja', iso: 'ja-JP', file: 'ja-JP.js'},
      ],
      defaultLocale: 'vi',
      vueI18n: {
        fallbackLocale: 'en',
      },
      lazy: true,
      langDir: 'lang/'
    }],
    '@nuxtjs/auth',
    ['@nuxtjs/component-cache', {maxAge: 1000 * 60 * 60}],
  ],
  auth: {
    redirect: {
      login: '/auth/login',
      logout: '/',
      callback: '/auth/login',
      home: '/'
    },
    strategies: {
      local: {
        endpoints: {
          login: {url: process.env.API_URL + '/auth/login', method: 'post', propertyName: 'token'},
          logout: {url: process.env.API_URL + '/auth/logout', method: 'post'},
          user: {url: process.env.API_URL + '/auth/user', method: 'get', propertyName: 'user'}
        },
        tokenRequired: true,
        tokenType: 'Bearer',
      }
    }
  },
  toast: {
    position: 'top-right'
  },
  // Important notice: Do not import actual styles. Use this module only to import variables, mixins, functions (et cetera) as they won't exist in the actual build.
  styleResources: {
    // your settings here
    sass: ['./assets/vars/*.scss', './assets/abstracts/_mixins.scss'], // alternative: scss, plz run npm i sass-loader node-sass
    less: [], // plz run npm i less-loader less
    stylus: [] // plz run npm i stylus-loader stylus -D
  },
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    // analyze: true,
    devtools: ({isDev}) => isDev,
    extractCSS: ({isDev}) => isDev,
    plugins: [
      new webpack.ProvidePlugin({
        // '$': 'jquery',
        '_': 'lodash'
      })
    ],
    extend(config, ctx) {
      config.module.rules.push({
        resourceQuery: /blockType=i18n/,
        type: "javascript/auto",
        loader: ["@kazupon/vue-i18n-loader", "yaml-loader"],
      });
    }
  }
}
