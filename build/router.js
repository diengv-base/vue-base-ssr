import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'

const _5ec704b2 = () => interopDefault(import('..\\pages\\master\\index.vue' /* webpackChunkName: "pages_master_index" */))
const _007f5bd4 = () => interopDefault(import('..\\pages\\sorry.vue' /* webpackChunkName: "pages_sorry" */))
const _f5705d10 = () => interopDefault(import('..\\pages\\auth\\login.vue' /* webpackChunkName: "pages_auth_login" */))
const _8f8696ee = () => interopDefault(import('..\\pages\\master\\address.vue' /* webpackChunkName: "pages_master_address" */))
const _325e796e = () => interopDefault(import('..\\pages\\master\\customer\\index.vue' /* webpackChunkName: "pages_master_customer_index" */))
const _0b458150 = () => interopDefault(import('..\\pages\\master\\customer\\add.vue' /* webpackChunkName: "pages_master_customer_add" */))
const _59c4f86e = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

Vue.use(Router)

if (process.client) {
  if ('scrollRestoration' in window.history) {
    window.history.scrollRestoration = 'manual'

    // reset scrollRestoration to auto when leaving page, allowing page reload
    // and back-navigation from other pages to use the browser to restore the
    // scrolling position.
    window.addEventListener('beforeunload', () => {
      window.history.scrollRestoration = 'auto'
    })

    // Setting scrollRestoration to manual again when returning to this page.
    window.addEventListener('load', () => {
      window.history.scrollRestoration = 'manual'
    })
  }
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected and scrollToTop is not explicitly disabled
  if (
    to.matched.length < 2 &&
    to.matched.every(r => r.components.default.options.scrollToTop !== false)
  ) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some(r => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise((resolve) => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}

export function createRouter() {
  return new Router({
    mode: 'history',
    base: decodeURI('/'),
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'exact-is-active',
    scrollBehavior,

    routes: [{
      path: "/en/master",
      component: _5ec704b2,
      name: "master___en"
    }, {
      path: "/master",
      component: _5ec704b2,
      name: "master___vi"
    }, {
      path: "/ja/master",
      component: _5ec704b2,
      name: "master___ja"
    }, {
      path: "/en/sorry",
      component: _007f5bd4,
      name: "sorry___en"
    }, {
      path: "/sorry",
      component: _007f5bd4,
      name: "sorry___vi"
    }, {
      path: "/ja/sorry",
      component: _007f5bd4,
      name: "sorry___ja"
    }, {
      path: "/en/auth/login",
      component: _f5705d10,
      name: "auth-login___en"
    }, {
      path: "/auth/login",
      component: _f5705d10,
      name: "auth-login___vi"
    }, {
      path: "/ja/auth/login",
      component: _f5705d10,
      name: "auth-login___ja"
    }, {
      path: "/en/master/address",
      component: _8f8696ee,
      name: "master-address___en"
    }, {
      path: "/master/address",
      component: _8f8696ee,
      name: "master-address___vi"
    }, {
      path: "/ja/master/address",
      component: _8f8696ee,
      name: "master-address___ja"
    }, {
      path: "/en/master/customer",
      component: _325e796e,
      name: "master-customer___en"
    }, {
      path: "/master/customer",
      component: _325e796e,
      name: "master-customer___vi"
    }, {
      path: "/ja/master/customer",
      component: _325e796e,
      name: "master-customer___ja"
    }, {
      path: "/en/master/customer/add",
      component: _0b458150,
      name: "master-customer-add___en"
    }, {
      path: "/master/customer/add",
      component: _0b458150,
      name: "master-customer-add___vi"
    }, {
      path: "/ja/master/customer/add",
      component: _0b458150,
      name: "master-customer-add___ja"
    }, {
      path: "/en/",
      component: _59c4f86e,
      name: "index___en"
    }, {
      path: "/",
      component: _59c4f86e,
      name: "index___vi"
    }, {
      path: "/ja/",
      component: _59c4f86e,
      name: "index___ja"
    }],

    fallback: false
  })
}
