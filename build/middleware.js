const middleware = {}

middleware['timing'] = require('@/middleware/timing.js');
middleware['timing'] = middleware['timing'].default || middleware['timing']

middleware['user-agent'] = require('@/middleware/user-agent.js');
middleware['user-agent'] = middleware['user-agent'].default || middleware['user-agent']

export default middleware
